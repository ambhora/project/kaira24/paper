% --------------------------------------------------------------------------------------------------
% SPDX-License-Identifier: CC-BY-4.0
% Copyright (C) 2024 Jayesh Badwaik <jayesh@ambhora.com>
% --------------------------------------------------------------------------------------------------
\makeatletter
\def\input@path{{./tex}}
\makeatother
\documentclass[twoside=false,11pt,a4paper]{pencharticle}

\setlength{\footheight}{10mm}
\RequirePackage[
bindingoffset=6mm,
headheight=10mm,
inner=20mm,
top=20mm,
bottom=25mm,
outer=17mm
]{geometry}

\RequirePackage{listings}
\lstset{
  basicstyle=\ttfamily,
}

\title{A Model for Declarative Command Line Arguments}
\author{Jayesh Badwaik}

\begin{document}
\pagestyle{main}
\maketitle
\begin{abstract}
\end{abstract}

\section{Notation}

In this article, we are going to use notation which is a mix of mathematics and
computer science. In order to make the notation clear, we describe our notation
in this section.


\section{A Survey of Current Usage}
\label{sec:survey}

In this article, we are only concered with the string-based parsing of the
command line arguments. We are not concerned with the how the command line
arguments relate to the input arguments to the program.

For example, in the \texttt{git commit} command, an option like \texttt{-m} is
used to specify the commit message, if such an option is not specified, the
user is prompted to enter the commit message in a text editor. For the purposes
of this article, the \texttt{-m} option is considered as a command line
argument, and the behavior of \texttt{git} in prompting the user for the commit
message is considered as an implementation detail of the program. In
particular, when the \texttt{-m} option is not specified, a correct result of
the parsing would be no commit message being specified.



\subsection{Flags}
These arguments are typically boolean in nature and
indicate the presence or absence of a feature. For example, the
\texttt{--verbose} flag is often used to indicate verbose output for the
program or a \texttt{--debug} flag is used to indicate that the program
should run in the debug mode. Apart from the basic usage, flags can also
show more complex usage. We list down a few examples of them below:

\begin{enumerate}

  \item The flags almost always have a default value. For example, the
    \texttt{--verbose} flag is typically off by default. The user has to
    explicitly specify the \texttt{--verbose} flag to turn on the verbose
    mode.

  \item
    Some programs also have short forms for flags. For example, the
    \texttt{-v} flag is often used as a short form for the \texttt{--verbose}
    flag.

  \item
    Some flags can be repeated multiple times. For example, the \texttt{-v}
    flag can be repeated multiple times to increase the verbosity of the output
    like \texttt{-vvv} or \texttt{-vvvv} in programs like \texttt{ssh}.

  \item
    Some programs also have negated flags. The negation is typically achieved
    by prefixing a \texttt{no-} to an existing flag name. For example, the
    \texttt{--no-verbose} flag is used to indicate that the program should not
    run in verbose mode.

\end{enumerate}


\subsection{Options}
These arguments are typically key-value pairs. For example, the
\texttt{--output a.out} option is used to specify \texttt{a.out} as the output
file for the program or the \texttt{--input b.in} option is used to specify
\texttt{b.in} as the input file for the program. In a similar way to flags,
options can also have complexities beyond their basic usage. We list down the
commonly accepted usages below:
\begin{enumerate}
  \item Unlike flags, but not always, optional arguments are more likely to not
    have a default value. In \texttt{git} for example, if no \texttt{-m} option
    is specified for the \texttt{git commit} command, no default commit
    message is used. Instead, the user is prompted to enter a commit message in
    a text editor.

  \item Just like flags, the options can have short forms. For example, the
    \texttt{-o a.out} option is a short form for the \texttt{--output a.out}
    option.

  \item The options can occur multiple times in a command line. For example,
    the \texttt{-I /usr/include} option can be repeated multiple times to
    specify multiple include directories in a C program.

  \item Some programs also have a style of multiple options in a single
    argument form. For example, \texttt{ansible-playbook} has the
    \texttt{--extra-vars} option which can be used to specify multiple
    variables in a single argument as a key-value pair.

    \texttt{ansible-playbook --extra-vars "var1=val1 var2=val2 var3=val3"}

  \item Sometimes the options cannot occur after or before a certain point in
    the command line. For example, with the \texttt{find} command, the
    options like \texttt{-name} or \texttt{-type} cannot occur before the path
    argument.


    A very common variant of this behavior is the \texttt{--} option. The
    \texttt{--} option is used to indicate the end of options and the beginning
    of positional arguments or arguments which is not to be parsed by the
    command line parser, but instead passed on to the program as a
    "passthrough" argument. For example in the \texttt{grep} command,
    the \texttt{--} option is used to indicate the end of options and the
    beginning of the search pattern. This is useful when the search pattern
    starts with a \texttt{-} character.

  \item Some programs also have a style of multiple flags and mixing flags and
    options in a single argument form. For example, the \texttt{tar} command
    allows a \texttt{-cvf filename} which is a combination of the flags
    \texttt{-c}, \texttt{-v}, and the option \texttt{-f filename}.


\end{enumerate}


\subsection{Positional Arguments}
These arguments are arguments that are not preceded by a flag or an option. For
these kind of arguments, the order in which they specified on the command line
is typically important. For example in the command \texttt{cp a.txt b.txt}, the
\texttt{a.txt} and \texttt{b.txt} are positional arguments. The order in which
they are specified is important. The \texttt{a.txt} file is the source file and
the \texttt{b.txt} file is the destination file.

Compared to the flags and options, positional arguments are less complex, but
they do also show some complexities. Most of these complexities are in terms of
the number of positional arguments that can be specified.

For example, there are programs like \texttt{cp} which allows any number of
positional arguments. The last positional argument is always the destination
path and the rest are the source paths. As is typically the case, there are
generally two kinds of positional  arguments, and at most one of them can be
repeated multiple times. Theoretically, there can possibly be three kinds of
positional arguments, with the middle one of the three being repeated multiple
times. However, in practice, we have not come across such a program.


\subsection{Subcommands}

Subcommands are generally used to manage the complexity of a large program by
breaking them down into smaller, more manageable parts. For example, the
\texttt{git} command has subcommands like \texttt{commit}, \texttt{push},
\texttt{pull}, etc. Each of these subcommands can have their own set of flags,
options, and positional arguments, and often can be treated as a full-fledged
command in their own right. The subcommands can also have their own subcommands.
Finally, the original command can also have its own set of flags and options. It
can be quite tricky implementation-wise for a command to accept both positional
arguments and subcommands. Such a construct also makes it hard for the user to
understand the command line interface of the program.


\section{The Model}






\appendix
\part{Appendices}

\end{document}
